﻿using Volo.Abp.Modularity;

namespace DemoAbp;

[DependsOn(
    typeof(DemoAbpApplicationModule),
    typeof(DemoAbpDomainTestModule)
    )]
public class DemoAbpApplicationTestModule : AbpModule
{

}
