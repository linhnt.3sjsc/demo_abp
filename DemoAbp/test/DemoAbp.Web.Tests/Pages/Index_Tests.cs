﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace DemoAbp.Pages;

public class Index_Tests : DemoAbpWebTestBase
{
    [Fact]
    public async Task Welcome_Page()
    {
        var response = await GetResponseAsStringAsync("/");
        response.ShouldNotBeNull();
    }
}
