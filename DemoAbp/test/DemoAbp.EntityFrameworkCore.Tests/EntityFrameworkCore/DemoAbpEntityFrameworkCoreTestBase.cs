﻿using Volo.Abp;

namespace DemoAbp.EntityFrameworkCore;

public abstract class DemoAbpEntityFrameworkCoreTestBase : DemoAbpTestBase<DemoAbpEntityFrameworkCoreTestModule>
{

}
