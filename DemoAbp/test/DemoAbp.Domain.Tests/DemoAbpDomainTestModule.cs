﻿using DemoAbp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace DemoAbp;

[DependsOn(
    typeof(DemoAbpEntityFrameworkCoreTestModule)
    )]
public class DemoAbpDomainTestModule : AbpModule
{

}
