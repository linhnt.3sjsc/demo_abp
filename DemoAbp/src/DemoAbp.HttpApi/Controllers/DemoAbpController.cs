﻿using DemoAbp.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace DemoAbp.Controllers;

/* Inherit your controllers from this class.
 */
public abstract class DemoAbpController : AbpControllerBase
{
    protected DemoAbpController()
    {
        LocalizationResource = typeof(DemoAbpResource);
    }
}
