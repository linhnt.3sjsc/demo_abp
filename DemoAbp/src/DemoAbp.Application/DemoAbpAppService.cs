﻿using System;
using System.Collections.Generic;
using System.Text;
using DemoAbp.Localization;
using Volo.Abp.Application.Services;

namespace DemoAbp;

/* Inherit your application services from this class.
 */
public abstract class DemoAbpAppService : ApplicationService
{
    protected DemoAbpAppService()
    {
        LocalizationResource = typeof(DemoAbpResource);
    }
}
