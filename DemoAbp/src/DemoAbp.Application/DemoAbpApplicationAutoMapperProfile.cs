﻿
using AutoMapper;
using DemoAbp.Authors;
using DemoAbp.Books;

namespace DemoAbp
{
    public class DemoAbpApplicationAutoMapperProfile : Profile
    {
        public DemoAbpApplicationAutoMapperProfile()
        {

            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
            CreateMap<Book, BookDto>();
            CreateMap<CreateUpdateBookDto, Book>();
            CreateMap<Author, AuthorDto>();
            CreateMap<Author, AuthorLookupDto>();
        }
    }

}

