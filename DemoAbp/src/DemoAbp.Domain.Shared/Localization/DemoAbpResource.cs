﻿using Volo.Abp.Localization;

namespace DemoAbp.Localization;

[LocalizationResourceName("DemoAbp")]
public class DemoAbpResource
{

}
