﻿namespace DemoAbp;

public static class DemoAbpDomainErrorCodes
{
    /* You can add your business exception error codes here, as constants */
    public const string AuthorAlreadyExists = "BookStore:00001";
}
