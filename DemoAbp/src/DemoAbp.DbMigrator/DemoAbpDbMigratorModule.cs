﻿using DemoAbp.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace DemoAbp.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(DemoAbpEntityFrameworkCoreModule),
    typeof(DemoAbpApplicationContractsModule)
    )]
public class DemoAbpDbMigratorModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
    }
}
