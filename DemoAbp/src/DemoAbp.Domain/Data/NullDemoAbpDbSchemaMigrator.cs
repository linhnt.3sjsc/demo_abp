﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace DemoAbp.Data;

/* This is used if database provider does't define
 * IDemoAbpDbSchemaMigrator implementation.
 */
public class NullDemoAbpDbSchemaMigrator : IDemoAbpDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
