﻿using System.Threading.Tasks;

namespace DemoAbp.Data;

public interface IDemoAbpDbSchemaMigrator
{
    Task MigrateAsync();
}
