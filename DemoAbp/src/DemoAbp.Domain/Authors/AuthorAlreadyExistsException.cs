﻿using Volo.Abp;

namespace DemoAbp.Authors
{
    public class AuthorAlreadyExistsException : BusinessException
    {
        public AuthorAlreadyExistsException(string name)
            : base(DemoAbpDomainErrorCodes.AuthorAlreadyExists)
        {
            WithData("name", name);
        }
    }
}
