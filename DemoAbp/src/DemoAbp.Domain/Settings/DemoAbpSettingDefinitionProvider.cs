﻿using Volo.Abp.Settings;

namespace DemoAbp.Settings;

public class DemoAbpSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        //Define your own settings here. Example:
        //context.Add(new SettingDefinition(DemoAbpSettings.MySetting1));
    }
}
