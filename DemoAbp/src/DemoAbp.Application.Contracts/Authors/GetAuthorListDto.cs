﻿
using Volo.Abp.Application.Dtos;

namespace DemoAbp.Authors
{
    public class GetAuthorListDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
