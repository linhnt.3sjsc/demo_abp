﻿using DemoAbp.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace DemoAbp.Permissions;

public class DemoAbpPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var bookStoreGroup = context.AddGroup(DemoAbpPermissions.GroupName, L("Permission:BookStore"));

        var booksPermission = bookStoreGroup.AddPermission(DemoAbpPermissions.Books.Default, L("Permission:Books"));
        booksPermission.AddChild(DemoAbpPermissions.Books.Create, L("Permission:Books.Create"));
        booksPermission.AddChild(DemoAbpPermissions.Books.Edit, L("Permission:Books.Edit"));
        booksPermission.AddChild(DemoAbpPermissions.Books.Delete, L("Permission:Books.Delete"));

        var authorsPermission = bookStoreGroup.AddPermission(
    DemoAbpPermissions.Authors.Default, L("Permission:Authors"));

        authorsPermission.AddChild(
            DemoAbpPermissions.Authors.Create, L("Permission:Authors.Create"));

        authorsPermission.AddChild(
            DemoAbpPermissions.Authors.Edit, L("Permission:Authors.Edit"));

        authorsPermission.AddChild(
            DemoAbpPermissions.Authors.Delete, L("Permission:Authors.Delete"));

    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<DemoAbpResource>(name);
    }
}
