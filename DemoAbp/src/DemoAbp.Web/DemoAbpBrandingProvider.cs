﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace DemoAbp.Web;

[Dependency(ReplaceServices = true)]
public class DemoAbpBrandingProvider : DefaultBrandingProvider
{
    public override string AppName => "DemoAbp";
}
