﻿using DemoAbp.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace DemoAbp.Web.Pages;

/* Inherit your PageModel classes from this class.
 */
public abstract class DemoAbpPageModel : AbpPageModel
{
    protected DemoAbpPageModel()
    {
        LocalizationResourceType = typeof(DemoAbpResource);
    }
}
